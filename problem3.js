// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function problem3(inventory)
{   let alfaModel=[]
   if((inventory) && (inventory.length))
   {
      for(var i=0;i<inventory.length;i++)
      {
         alfaModel.push(inventory[i]['car_model'].toUpperCase())
      }
      return alfaModel.sort()
   }
   else
   {
      return alfaModel
   }
}
module.exports=problem3;