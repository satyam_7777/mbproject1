// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  
//Execute a function and return an array that only contains BMW and Audi cars.  
//Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function problem6(inventory)
{let BMWAndAudi =[];
    if((inventory)&&(inventory.length))
   {
    for (let j = 0; j< inventory.length; j++) {
        if (inventory[j]['car_make'] == 'BMW' || inventory[j]['car_make'] == 'Audi') 
        {
            BMWAndAudi.push(inventory[j]);
        }
    }
    return BMWAndAudi;
}
else
{
    return BMWAndAudi;
}
}
module.exports=problem6;
